<?php

function confirm_checkout ($private_key, $order_id, $use_sandbox = true) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://api.aplazame.com/orders/$order_id/authorize");
	curl_setopt($ch, CURLOPT_POST, true);
	// curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

	// esto es para no incluir la cabecera de la respuesta
	curl_setopt($ch, CURLOPT_HEADER, false);

	// devuelve la respuesta como string en lugar de imprimirlo directamente
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// cabeceras con modo y credenciales
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Accept: application/vnd.aplazame'.( $use_sandbox ? '.sandbox' : '' ).'.v1+json',
    'Authorization: Bearer '.$private_key,
    'Host: api.aplazame.com'
	) );

	// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);

	// curl_setopt($ch, CURLOPT_VERBOSE, 1);

	$data = curl_exec($ch);

	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	$result = array(
		'config' => curl_getinfo($ch),
		'status' => $status,
		'data' => json_decode($data),
		'succeeded' => ( $status >= 200 && $status < 400 )
	);

	curl_close($ch);

	return $result;
}
