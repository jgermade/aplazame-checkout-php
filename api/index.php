<?php

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/aplazame_checkout.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

$app = new Silex\Application();
$private_key = getenv('APLAZAME_PRIVATE_KEY');

$app->before(function (Request $request) {
  if( 0 === strpos($request->headers->get('Content-Type'), 'application/json') ) {
    $data = json_decode($request->getContent(), true);
    $request->request->replace(is_array($data) ? $data : array());
  }
});

$app->get('/api/info', function(Request $request) use($app) {
	phpinfo();
});

$app->post('/api/confirm_checkout', function(Request $request) use($app, $private_key) {

    $token = $request->request->get('checkout_token');

	$result = confirm_checkout( $private_key, $token );

    file_put_contents("../orders.log", "order: $token\ntime: ".date("Y-m-d H:i:s")."\nresult:\n".json_encode($result,JSON_PRETTY_PRINT)."\n\n", FILE_APPEND | LOCK_EX);

  if( $result['status'] >= 500 ) {
    return $app->json($result,502);
  }
	if( !$result['succeeded'] ) {
    return $app->json($result,400);
	}

  // debe comprobarse que la cantidad no ha cambiado
  if( $result['data']->amount !== 13014 ) {
    return $app->json([ 'reason' >= 'order amount does not match' ],400);
  }

  return $app->json($result,200); // 204 = OK, sin devolver datos
});

$app->run();
